FROM neverlanctf/base:4th-year

MAINTAINER Zane Durkin <zane@neverlanctf.org>

RUN apk add --update nodejs nodejs-npm

WORKDIR /var/www/

COPY . /var/www/

RUN npm install

RUN npm run build

RUN cp -r build/*  html/

EXPOSE 80
