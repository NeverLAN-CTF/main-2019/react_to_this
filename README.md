# React To This

#### Description
It looks like someone setup their react site wrong...

#### Flag
flag{s3cur3_y0ur_s3ss10ns}

#### Hint
Cookies aren't the only thing that you can a browser will save


## Development

#### Setting flag
Flag is set in the `src/Pages/Admin.js` file

#### Running a development server

First update the node_modules

    npm install

Next, run the server

    npm start

you're all set


#### Deploying in docker

Build the docker container

    docker build -t react_to_this .

Run the container and attach a port

    docker run -dit -p 8080:80 react_to_this
