import React, { Component } from 'react'
import { connect } from "react-redux";
import { mapState, mapDispatch } from '../store/actions';
import {
  Navbar,
  NavbarBrand,
  NavbarItem,
  NavbarBurger,
  NavbarMenu,
  NavbarDivider,
  NavbarStart,
  NavbarEnd,
  NavbarLink,
  NavbarDropdown,

} from 'bloomer';
// The Header creates links that can be used to navigate
// between routes.
class HeaderClass extends Component {
  constructor(props){
    super(props);
    //console.log(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = {isActive: false};
  }
  handleToggle(e){
	  this.setState({isActive: !this.state.isActive});
  }

  render() {
    var admin = this.props.admin?<NavbarItem href='/admin/'>Admin Page</NavbarItem>:"";
    return (
      <Navbar style={{ border: 'solid 1px #00D1B2', margin: '0' }}>
        <NavbarBrand>
            <NavbarItem>
              React To This
            </NavbarItem>
            <NavbarBurger isActive={this.state.isActive} onClick={this.handleToggle} />
        </NavbarBrand>
        <NavbarMenu isActive={this.state.isActive} onClick={this.handleToggle}>
            <NavbarStart>
                <NavbarItem href='/home/'>Home</NavbarItem>
                <NavbarItem hasDropdown isHoverable>
                    <NavbarLink href='#/documentation'>DropDown</NavbarLink>
                    <NavbarDropdown>
                        <NavbarItem href='#/'>One A</NavbarItem>
                        <NavbarItem href='#/'>Two B</NavbarItem>
                        <NavbarDivider />
                        <NavbarItem href='#/'>Two A</NavbarItem>
                    </NavbarDropdown>
                </NavbarItem>
            </NavbarStart>
            <NavbarEnd>
              {admin}
            </NavbarEnd>
        </NavbarMenu>
      </Navbar>
      );
  }
}
var Header = connect(
  mapState,
  mapDispatch
)(HeaderClass);
export default Header
