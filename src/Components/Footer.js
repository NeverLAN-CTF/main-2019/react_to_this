import React from 'react'
import { connect } from "react-redux";
import { mapState, mapDispatch } from '../store/actions';
import { Tabs, TabList, Tab, TabLink} from 'bloomer';

class FooterClass extends React.Component {
  render(){
    return (
  <footer>
    <Tabs isAlign="centered">
      <TabList align="center" horizontal>
        <Tab><TabLink href='#'>Here is my footer</TabLink></Tab>
      </TabList>
    </Tabs>
  </footer>
  );
  }
}

var Footer = connect(
  mapState,
  mapDispatch
)(FooterClass);

export default Footer;
