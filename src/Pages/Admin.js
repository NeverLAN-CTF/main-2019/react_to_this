import React from 'react'
import { connect } from "react-redux";
import { mapState, mapDispatch } from '../store/actions';
// Import with global scoped class names
import { Columns, Column } from 'bloomer';
//import Logo from "./Assets/img/icon.png";

class AdminClass extends React.Component {
  render(){
    var admin = 'You are not an admin.';
    var flg = 's3cur3_y0ur_s3ss10ns';
    if (this.props.admin){
      admin = <React.Fragment>
        <div className="banner" align="center">
          <h1>Welcome to the Admin Page!</h1>
        </div>
        <Columns>
          <Column align="center"><p>f{'l'}a{'g'}{'{'+flg+'}'}</p></Column>
        </Columns>
      </React.Fragment>;
    }else{
      window.history.back();
    }
    return (admin);
  }
}
var Admin = connect(
  mapState,
  mapDispatch
)(AdminClass);

export default Admin
