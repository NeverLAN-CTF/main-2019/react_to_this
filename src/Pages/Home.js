import React from 'react'
// Import with global scoped class names
import { Columns, Column } from 'bloomer';
//import Logo from "./Assets/img/icon.png";

const Home = () => (
  <div>
		<div className="banner" align="center">
		  <h1>Welcome to my website!</h1>
		</div>
		<Columns align="center">
		  <Column><p>Hello World!</p></Column>
		</Columns>
  </div>
)

export default Home
