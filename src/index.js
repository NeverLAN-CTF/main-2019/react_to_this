import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'
import { BrowserRouter } from 'react-router-dom'
import App from './app';

import registerServiceWorker from './registerServiceWorker';


// Store
import configureStore from './store/configureStore'

const { persistor, store } = configureStore();




ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </PersistGate>
  </Provider>
  ,
  document.getElementById('root')
);
registerServiceWorker();
