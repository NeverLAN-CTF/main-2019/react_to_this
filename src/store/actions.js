//Actions
// Map Redux state to component props
export function mapState(state) {
  return {
    admin: state.admin
  }
}

// Action
var logoutAction = { type: "logout" };
var loginAction = { type: 'login'};

// Map Redux actions to component props
export function mapDispatch(dispatch) {
  return {
	login: function() {
      return dispatch(loginAction);
    },
	logout: function() {
      return dispatch(logoutAction);
    }
  }
}
