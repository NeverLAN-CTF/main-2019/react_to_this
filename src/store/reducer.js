// Reducer
function profile(state, action) {
  if (state === undefined) {
    return {
      admin: false
    };
  }

 // var user = state.user;

  switch (action.type) {
    case "login":
	  return {admin:true};
    case "logout":
      return { admin:false };
    default:
      return state;
  }
}

export default profile;
