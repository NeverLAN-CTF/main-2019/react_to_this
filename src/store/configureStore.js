// configureStore.js
import { createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' // defaults to localStorage for web and AsyncStorage for react-native

import profile from "./reducer";

const persistConfig = {
  key: 'root',
  storage,
}

const initialState = {
  admin:false
};

const persistedReducer = persistReducer(persistConfig, profile);

export default () => {
  let store = createStore(persistedReducer, initialState)
  let persistor = persistStore(store)
  return { store, persistor }
}
