import React, {Component} from 'react'
import { Switch, Route } from 'react-router-dom'

// Assets
import './Assets/css/main.css'

// Components
import Header from './Components/Header.js'
import Footer from './Components/Footer.js'

// Pages
import Home from './Pages/Home.js'
import Admin from './Pages/Admin.js'

// build app
export default class App extends Component{


  render(){
    return (
      <React.Fragment>
        <Header />
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/home' component={Home}/>
          <Route path='/admin' component={Admin}/>
    	    <Route component={Home}/>
        </Switch>
        <Footer />
      </React.Fragment>
    );
  }
}
